import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public abstract class Transporter<T extends Vehicle> extends Vehicle implements Carrier {
    boolean rampDown = false;
    protected List<T> container;
    protected int nrVehicles=0;
    private int maxCars;

    Transporter(Color color, double posX, double posY, double dir, int maxCars) {
        super(color, posX, posY, dir);
        this.container = new ArrayList<>(maxCars);
        this.maxCars = maxCars;
    }
    public void lowerRamp() {
        if(currentSpeed == 0) {
            rampDown = true;
        }
    }
    public void raiseRamp() {
        rampDown = false;
    }

    public void load(T v) {
        if(nrVehicles < maxCars && isNear(v.posX,v.posY) && rampDown && v != this){
            container.add(v);
            v.drivable=false;
            nrVehicles++;
        }
    }

    public abstract void unload();

    @Override
    protected double speedFactor() {
        return 0;
    }
}
