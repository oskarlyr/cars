import java.awt.*;

public class Scania extends Car implements Carrier {
    private double flatbed=0;

    Scania(Color color, double posX, double posY, double dir, int flatbed) {
        super(color, posX, posY, dir);
        this.nrDoors = 2;
        this.enginePower = 1000;
        this.modelName = "Scania";
    }

    @Override
    protected double speedFactor() {
        return 0;
    }

    @Override
    public void raiseRamp() {
        if (flatbed == 0) {
            flatbed = Math.min(flatbed + 10, 70);
        }
    }

    @Override
    public void lowerRamp() {
        if (flatbed == 0) {
            flatbed = Math.max(flatbed - 10, 0);
        }
    }
}
