import java.awt.*;
import static java.lang.StrictMath.abs;

public abstract class Vehicle implements Movable {
    protected String modelName; // The car model name
    protected int nrDoors; // Number of doors on the car
    protected double enginePower; // Engine power of the car
    protected double currentSpeed = 0; // The current speed of the car
    protected Color color; // Color of the car
    protected double posX;
    protected double posY;
    protected double dir=0;
    protected boolean drivable=true;

    Vehicle(Color color, double posX, double posY, double dir){
        this.color = color;
        this.posX = posX;
        this.posY = posY;
        this.dir = dir;
        stopEngine();
    }

    /**
     * Changes direction according to position and speed.
     */
    public void move(){
        dir %= tau;
        dir = Math.round(dir);
        posY += Math.sin(dir) * currentSpeed;
        posX += Math.cos(dir) * currentSpeed;
    }

    /**
     * Changes direction of car 30 degree to left.
     */
    public void turnLeft(){
        dir -= 12/tau;
    }
    /**
     * Changes direction of car 30 degree to right.
     */
    public void turnRight(){
        dir += 12/tau;
    }
    /**
     * Return number of doors.
     * @return nrDoors
     */
    public int getNrDoors(){
        return nrDoors;
    }
    /**
     * Return engine power.
     * @return enginePower
     */
    public double getEnginePower(){
        return enginePower;
    }

    /**
     * Return current speed.
     * @return currentSpeed
     */
    public double getCurrentSpeed(){
        return currentSpeed;
    }

    /**
     * Return color of car.
     * @return color
     */
    public Color getColor(){
        return color;
    }
    /**
     * Set color of car.
     * @param clr (input Color)
     */
    public void setColor(Color clr){
        color = clr;
    }
    /**
     * Starts car.
     */
    public void startEngine(){
        currentSpeed = 0.1;
    }
    /**
     * Stops car.
     */
    public void stopEngine(){
        currentSpeed = 0;
    }

    /**
     * Adds gas to the tank.
     * @param amount (between interval [0,1])
     */
    public void gas(double amount){
        if (amount >= 1 && amount <= 0) {
            incrementSpeed(amount);
        }
        if (getCurrentSpeed() > getEnginePower()) {
            currentSpeed = getEnginePower();
        }
    }

    /**
     * Brake the car by intensity.
     * @param amount (between interval [0,1])
     */
    public void brake(double amount){
        if (amount <= 1 && amount >= 0) {
            decrementSpeed(amount);
        }
    }

    /**
     * Increase (but never decrease) speed
     * @param amount
     */
    protected void incrementSpeed(double amount){
        currentSpeed = Math.min(currentSpeed + speedFactor() * amount,enginePower);
    }

    /**
     * Decrease (never increase).
     * @param amount
     */
    protected void decrementSpeed(double amount){
        currentSpeed = Math.max(currentSpeed - speedFactor() * amount,0);
    }

    boolean isNear(double x, double y){
        return abs(posX-x) < 5 && abs(posY-y) < 5;
    }

    protected abstract double speedFactor();
}
