import java.awt.*;

/**
 * An instance of Car, namely a Saab 95. This car has a specific
 * exterior and engine (with turbo).
 */

public class Saab95 extends Car {
    private boolean turboOn = false;

    Saab95(Color color, double posX, double posY, double dir) {
        super(color, posX, posY, dir);
        this.modelName = "Saab95";
        this.nrDoors = 2;
        this.enginePower = 125;
    }

    /**
     * Turn turbo on.
     */
    public void setTurboOn() {
        turboOn = true;
    }

    /**
     * Turn off turbo.
     */
    public void setTurboOff() {
        turboOn = false;
    }

    protected double speedFactor() {
        double turbo = 1;
        if (turboOn) turbo = 1.3;
        return enginePower * 0.01 * turbo;
    }
}