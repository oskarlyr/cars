import java.awt.*;

/**
 * An instance of Car, namely a Volvo 240. This car has a specific
 * exterior and engine.
 */

public class Volvo240 extends Car {

    private final static double trimFactor = 1.25;

    Volvo240(Color color, double posX, double posY, double dir) {
        super(color, posX, posY, dir);
        this.nrDoors = 4;
        this.enginePower = 100;
        this.modelName = "Volvo240";
    }
    
    protected double speedFactor(){
        return enginePower * 0.01 * trimFactor;
    }

}
