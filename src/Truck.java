import java.awt.*;

public class Truck extends Transporter<Car> {
    public Truck(Color color, double posX, double posY, double dir) {
        super(color, posX, posY, dir, 10);
        this.modelName = "Longtrader2000";
        this.enginePower = 2000;
        this.color = Color.DARK_GRAY;
    }

    public void unload(){
        if(rampDown){
            Car c = container.get(container.size()-1);
            c.drivable=true;
            c.posX = posX+1;
            c.posY = posY+1;
            container.remove(container.size()-1);
            nrVehicles--;
        }
    }
}
