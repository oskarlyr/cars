public interface Carrier {
    void raiseRamp();
    void lowerRamp();
}
