public interface Movable {
    double tau = Math.PI * 2;
    void move();
    void turnLeft();
    void turnRight();
}
