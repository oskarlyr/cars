import java.awt.*;

public class Ferry extends Transporter<Vehicle> {
    public Ferry(Color color, double posX, double posY, double dir, int maxCars) {
        super(color, posX, posY, dir, 20);
        this.modelName = "Ferrytail";
        this.enginePower = 500;
        this.color = Color.WHITE;

    }
    public void unload(){
        if(rampDown){
            Vehicle v = container.get(0);
            v.drivable=true;
            v.posX = posX+1;
            v.posY = posY+1;
            container.remove(container.get(0));
            nrVehicles--;
        }
    }

}
