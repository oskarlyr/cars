import java.awt.*;

public abstract class Car extends Vehicle {

    public Car(Color color, double posX, double posY, double dir) {
        super(color, posX, posY, dir);
    }
}
