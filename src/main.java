import java.awt.*;

public class main {
    public static void main(String []args){
        Volvo240 volvo = new Volvo240(Color.BLACK, 0,0,0);
        Saab95 saab = new Saab95(Color.YELLOW, 0,0,0);

        saab.incrementSpeed(2);
        volvo.incrementSpeed(2);
    }
}
