/**
 * Created by oskar on 2017-01-25.
 */

public interface testCar {
    void stopEngine();
    void startEngine();
    void testIncrement();
    void testDecrement();
    void turnLeft();
    void turnRight();
    void testMove();
    void testColor();
}
