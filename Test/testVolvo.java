import org.junit.Before;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

import java.awt.*;

public class testVolvo implements testCar {
    private Volvo240 volvo;

    @Before
    public void init() {
        volvo = new Volvo240();
    }

    @Test
    public void testIncrement() {
        double speed = volvo.getCurrentSpeed();
        volvo.incrementSpeed(1.0);
        assertTrue(speed < volvo.getCurrentSpeed());
    }

    @Test
    public void testDecrement() {
        volvo.incrementSpeed(2.0);
        double speed = volvo.getCurrentSpeed();
        volvo.decrementSpeed(1.0);
        assertTrue(speed > volvo.getCurrentSpeed());
    }

    @Override
    @Test
    public void turnLeft() {
        double oldDir = volvo.dir;
        volvo.turnLeft();
        assertTrue(oldDir > volvo.dir);
    }

    @Override
    @Test
    public void turnRight() {
        double oldDir = volvo.dir;
        volvo.turnRight();
        assertTrue(oldDir < volvo.dir);
    }

    @Override
    @Test
    public void testMove() {
        volvo.startEngine();
        volvo.dir = volvo.tau/12;
        double locationX = volvo.posX;
        double locationY = volvo.posY;
        volvo.move();
        assertTrue(locationX != volvo.posX && locationY != volvo.posY);
    }

    @Override
    @Test
    public void testColor() {
        volvo.setColor(Color.blue);
        volvo.setColor(Color.green);
        assertTrue(volvo.color == Color.green);
    }

    @Override
    @Test
    public void stopEngine() {
        volvo.incrementSpeed(1);
        volvo.stopEngine();
        assertTrue(volvo.getCurrentSpeed() == 0);
    }

    @Override
    @Test
    public void startEngine() {
        double speed = volvo.getCurrentSpeed();
        volvo.startEngine();
        assertTrue(volvo.getCurrentSpeed() > 0 && speed == 0);
    }
}
