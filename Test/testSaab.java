import org.junit.Test;

/**
 * Created by oskar on 2017-01-25.
 */

import org.junit.Before;
import static org.junit.Assert.*;
import org.junit.Test;

import java.awt.*;

public class testSaab implements testCar {
    private Saab95 saab;
    Car c2 = new Saab95();
    Car c = new Scania();

    @Before
    public void init() {
        saab = new Saab95();
    }


    @Test
    public void testScania(){
        c.startEngine();
    }
    @Override
    @Test
    public void stopEngine() {
        saab.incrementSpeed(1);
        saab.stopEngine();
        assertTrue(saab.getCurrentSpeed() == 0);
    }

    @Override
    @Test
    public void startEngine() {
        double speed = saab.getCurrentSpeed();
        saab.startEngine();
        assertTrue(saab.getCurrentSpeed() > speed);
    }

    @Override
    @Test
    public void testIncrement() {
        double speed = saab.getCurrentSpeed();
        saab.incrementSpeed(0.00000001);
        assertTrue(speed < saab.getCurrentSpeed());
    }

    @Override
    @Test
    public void testDecrement() {
        saab.incrementSpeed(2.0);
        double speed = saab.getCurrentSpeed();
        saab.decrementSpeed(1.0);
        assertTrue(speed > saab.getCurrentSpeed());
    }

    @Override
    @Test
    public void turnLeft() {
        double oldDir = saab.dir;
        saab.turnLeft();
        assertTrue(oldDir > saab.dir);
    }

    @Override
    @Test
    public void turnRight() {
        double oldDir = saab.dir;
        saab.turnRight();
        assertTrue(oldDir < saab.dir);

    }

    @Override
    @Test
    public void testMove() {
        saab.startEngine();
        saab.dir = saab.tau/12;
        double oldPosX = saab.posX;
        double oldPosY = saab.posY;
        saab.move();
        assertTrue(saab.posX != oldPosX && saab.posY != oldPosY);
    }

    @Override
    @Test
    public void testColor() {
        saab.setColor(Color.green);
        saab.setColor(Color.black);
        assertTrue(saab.getColor() == Color.black);
    }

    public void testTurbo() {
        saab.setTurboOn();
        assertTrue(saab.turboOn);
        saab.setTurboOff();
        assertTrue(!saab.turboOn);
    }
}
