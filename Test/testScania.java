/**
 * Created by oskar on 2017-01-30.
 */
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class testScania {
    private Scania scan;

    @Before
    public void init() {
        scan = new Scania();
    }

    @Test
    public void startWithFlatbed() {
        scan.increaseFlatbed();
        scan.startEngine();
        assertTrue(scan.getCurrentSpeed() == 0 && scan.getFlatbed() != 0);

    }
}
